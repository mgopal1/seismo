package seismo.app.api;

import android.util.Log;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import seismo.app.SiesmoApplication;


/**
 * Created by manoj on 01/12/2014.
 * Class for API Methods
 */
public class API<T> {

    private static final String TAG = "API";
    private static API instance;
    private RequestQueue mRequestQueue;

    public API() {
    }

    /**
     *  Singleton
     * @return
     */
    public static API getInstance() {
        if (instance == null) {
            instance = new API();
        }

        return instance;
    }


    public void executeJSONMethod(int method, String url, Class<T> clazz, Updater<T> callback) {

        GsonRequest<T> jsonRequest = new GsonRequest<T>(method, url, clazz, callback, callback);
        try {
            Log.d(TAG, "API: " + jsonRequest.getUrl() + ", headers: " + new Gson().toJson(jsonRequest.getHeaders()));
        } catch (Exception e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }

        SiesmoApplication.getInstance().getRequestQueue().add(jsonRequest);
    }

    /**
     * Volley Request to parse and convert response into model via gson marshalling
     * @param <T>
     */
    private class GsonRequest<T> extends Request<T> {

        private final Response.Listener<T> callback;
        private final Class<T> clazz;

        public GsonRequest(int method, String url, Class<T> clazz, Response.Listener<T> callback, Response.ErrorListener errorCallback) {
            super(method, url, errorCallback);
            this.callback = callback;
            this.clazz = clazz;
        }

        @Override
        protected Response<T> parseNetworkResponse(NetworkResponse response) {
            try {
                String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new Gson().fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
            } catch (Exception e) {
                return Response.error(new ParseError(e));
            }
        }

        @Override
        protected void deliverResponse(T response) {
            if (callback != null) {
                callback.onResponse(response);
            }
        }
    }

    public void logError(VolleyError error) {

        if (error != null) {
            Gson gson = new Gson();
            Log.e(TAG, error.getMessage() + " [ " + (error.networkResponse != null ?
                    "status: " + error.networkResponse.statusCode +
                            ", data: " + error.networkResponse.data.toString() + ", headers: " +
                            gson.toJson(error.networkResponse.headers) : "") + " ]");
        }

    }
}

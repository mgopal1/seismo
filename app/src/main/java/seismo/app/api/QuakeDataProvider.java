package seismo.app.api;

import com.android.volley.Request;
import seismo.app.models.quake.QuakeData;

/**
 * Created by manoj on 01/12/2014.
 *
 * API Specific to quake data
 */
public class QuakeDataProvider {

    public static void getOccurences(Integer max, String minMagnitude, Updater<QuakeData> callback) {

        String url = "http://www.seismi.org/api/eqs?limit=" + (max != null ? max.toString() : "10") + "&min_magnitude=" + (minMagnitude != null ? minMagnitude : "");

        API.getInstance().executeJSONMethod(Request.Method.GET, url, QuakeData.class, callback);

    }
}

package seismo.app.api;

import com.android.volley.Request;
import com.android.volley.Response;

/**
 * Created by manoj on 01/12/2014.
 * Call back interface
 */
public interface Updater<T> extends Response.Listener<T>, Response.ErrorListener {
}

package seismo.app.ui;

/**
 * Created by manoj on 01/12/2014.
 */
public interface ContentUpdateInterface {

    public void loadData();

    public void bindViews();
}

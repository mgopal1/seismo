package seismo.app.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.android.volley.VolleyError;
import seismo.app.R;
import seismo.app.api.API;
import seismo.app.api.QuakeDataProvider;
import seismo.app.api.Updater;
import seismo.app.models.quake.QuakeData;
import seismo.app.models.quake.QuakeItem;

import java.util.List;

public class QuakeListFragment extends Fragment implements ContentUpdateInterface {

    private static final String TAG = "QuakeListFragment";
    @InjectView(R.id.quake_list)
    ListView listView;

    @InjectView(R.id.loading)
    LinearLayout loadingView;

    @InjectView(R.id.min_magintude)
    EditText minMagnitudeView;

    private QuakeData quakeData;

    private ActionBarActivity mListener;
    private View layout;
    private QuakeListAdapter mAdapter;
    private int limit = 10;
    private int offset = 0;
    private String minMagnitude;
    private int position = 0;


    public QuakeListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            quakeData = savedInstanceState.containsKey("quakeData") ? (QuakeData) savedInstanceState.getParcelable("quakeData") : null;
            position = savedInstanceState.getInt("position", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_quake_list, container, false);

        ButterKnife.inject(this, layout);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getLastVisiblePosition() + 2 > totalItemCount && loadingView.getVisibility() != View.VISIBLE) {
                    offset += limit;
                    position = listView.getLastVisiblePosition() - 3;
                    loadData();
                }
            }
        });

        minMagnitudeView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideKeyboard();
                update();
                return true;
            }
        });

        return layout;
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) ((Activity) mListener).getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(minMagnitudeView.getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();

        hideKeyboard();

        loadData();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ActionBarActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void update() {
        offset = 0;
        minMagnitude = minMagnitudeView.getText().toString();
        position = 0;
        loadData();
    }

    @Override
    public void loadData() {

        showView(loadingView, true);

        QuakeDataProvider.getOccurences(limit + offset, minMagnitude, new Updater<QuakeData>() {
            @Override
            public void onErrorResponse(VolleyError error) {
                API.getInstance().logError(error);
            }

            @Override
            public void onResponse(QuakeData response) {
                quakeData = response;
                bindViews();
            }
        });
    }

    @Override
    public void bindViews() {
        if (quakeData != null && quakeData.results != null && quakeData.results.size() > 0) {
            // List
            mAdapter = new QuakeListAdapter((Activity) mListener, R.layout.quake_item, quakeData.results);
            listView.setAdapter(mAdapter);
//            listView.setSelection(position);
        } else {
            Toast.makeText(null, "No occurances found", Toast.LENGTH_LONG).show();
        }

        showView(loadingView, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("quakeData", quakeData);
        outState.putInt("position", position);
    }

    public QuakeData getData() {
        return quakeData;
    }

    private class QuakeListAdapter extends ArrayAdapter<QuakeItem> {

        private final Context context;

        public QuakeListAdapter(Context context, int resource, List<QuakeItem> objects) {
            super(context, resource, objects);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;
            ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = LayoutInflater.from(context).inflate(R.layout.quake_item, parent, false);
                holder.regionView = (TextView) view.findViewById(R.id.region);
                holder.magnitudeView = (TextView) view.findViewById(R.id.magnitude);
                holder.mapView = (ImageButton) view.findViewById(R.id.map_view);
                holder.depthView = (TextView) view.findViewById(R.id.depth);
                holder.timeView = (TextView) view.findViewById(R.id.time);
                holder.colourView = view.findViewById(R.id.colour);

                holder.mapView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMapView((QuakeItem) v.getTag());
                    }
                });

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            QuakeItem item = getItem(position);

            holder.regionView.setText(item.region);
            holder.magnitudeView.setText(item.magnitude);

            try {
                float scale = (Float.parseFloat(item.magnitude));

//                holder.colourView.setBackgroundColor(Color.argb(255, (int) (255 * (9-scale) / 9), (int)(255 * scale / 9), 0));

//                holder.magnitudeView.setTextSize(12 * scale);
//                Log.d(TAG, "Scale: " + scale);
            } catch (Exception e) {
                Log.e(TAG, "Error: " + e.getMessage());
            }

            holder.depthView.setText(item.depth + " km");
            holder.timeView.setText(item.timedate);
            holder.mapView.setTag(item);

            return view;
        }

        private class ViewHolder {
            TextView regionView;
            TextView magnitudeView;
            ImageButton mapView;
            TextView depthView;
            TextView timeView;
            View colourView;
        }
    }

    private void showView(View v, boolean flag) {
        if (v != null) {
            v.setVisibility(flag ? View.VISIBLE : View.GONE);
        }
    }

    private void showMapView(QuakeItem item) {
        // Launch new Dialog Frag and show Map View
        QuakeLocationFragment fragment = QuakeLocationFragment.newInstance(item);
        fragment.show(((ActionBarActivity) mListener).getSupportFragmentManager(), "mapview");

    }
}

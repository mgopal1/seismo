package seismo.app.ui;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import seismo.app.MainActivity;
import seismo.app.R;
import seismo.app.models.quake.QuakeData;
import seismo.app.models.quake.QuakeItem;

public class QuakeLocationFragment extends DialogFragment implements ContentUpdateInterface {
    @InjectView(R.id.title)
    TextView titleView;

    @InjectView(R.id.cancel_button)
    Button cancelButton;

    @InjectView(R.id.display_button)
    Button displayButton;

    private static final String TAG = "QuakeLocationFragment";
    private static final int GOOGLE_PLAY_SERVICES_RESOLUTION = 7687;
    private OnFragmentInteractionListener mListener;
    private QuakeItem quakeItem;
    private View layout;
    private MapView mapView;
    private MainActivity context;
    private GoogleMap map;
    private int index =0;
    private GoogleMap.CancelableCallback callback;

    public static QuakeLocationFragment newInstance(QuakeItem item) {
        QuakeLocationFragment fragment = new QuakeLocationFragment();
        Bundle args = new Bundle();
        args.putParcelable("item", item);
        fragment.setArguments(args);
        return fragment;
    }

    public QuakeLocationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments() != null ? getArguments() : savedInstanceState;
        if (bundle != null) {
            quakeItem = bundle.containsKey("item") ? (QuakeItem) bundle.getParcelable("item") : null;
        }

        checkForPlayServices();

        try {
            MapsInitializer.initialize(context);
        } catch (Exception e) {
            Log.e(TAG, "Error initialsing map : " + e.getMessage());
        }
    }

    private boolean checkForPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, context, GOOGLE_PLAY_SERVICES_RESOLUTION).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_quake_location, container, false);
        ButterKnife.inject(this, layout);

        mapView = (MapView) layout.findViewById(R.id.map);
        mapView.onCreate(null);

        return layout;
    }

    @OnClick(R.id.cancel_button)
    public void cancel() {
        dismiss();
    }

    @OnClick(R.id.display_button)
    public void toggleDisplayAll() {
        if ( !displayButton.isActivated() ) {
            // Showing All
            // Load data and then display the first 10 results
            // and zoom out to world
            final QuakeData data = context.getData();
            LatLng latlng = null;
            index = 0;
            callback = new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    index++;
                    if ( index >= data.results.size() ) {
                        displayButton.setText("Show All");
                        displayButton.setActivated(false);
                        return ;
                    }
                    final QuakeItem item = data.results.get(index);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    moveCamera(addMarkerToMap(item), 2000, callback);
                                    if ( item != null && titleView != null )
                                        titleView.setText(item.region + ", (" + item.magnitude + ") @ depth of " + item.depth + " - time: " + item.timedate);
                                }
                            });
                        }
                    }, 2000);
                }

                @Override
                public void onCancel() {
                    index = data.results.size();
                }
            };

            QuakeItem item = data.results.get(index);
            titleView.setText(item.region + ", (" + item.magnitude + ") @ depth of " + item.depth + " - time: " + item.timedate);
            moveCamera(addMarkerToMap(item), 2000, callback);

            displayButton.setText("Show One");
            displayButton.setActivated(true);
        } else {
            // Showing One
            // Zoom into single one
            map.clear();

            callback.onCancel();

            moveCamera(addMarkerToMap(quakeItem), 1000, null);
            titleView.setText(quakeItem.region + ", (" + quakeItem.magnitude + ") @ depth of " + quakeItem.depth + " - time: " + quakeItem.timedate);

            displayButton.setText("Show All");
            displayButton.setActivated(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mapView.onResume();

        bindViews();
    }

    @Override
    public void loadData() {

    }

    @Override
    public void bindViews() {

        titleView.setText(quakeItem.region + ", (" + quakeItem.magnitude + ") @ depth of " + quakeItem.depth + " - time: " + quakeItem.timedate);

        if (mapView != null) {
            map = mapView.getMap();

            if (map != null) {
                map.getUiSettings().setMyLocationButtonEnabled(false);
                map.setMyLocationEnabled(true);
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                moveCamera(addMarkerToMap(quakeItem), 6, null);
            }
        }
    }


    /**
     * Add a circle top the map indicating location of quake
     *
     * @param item
     * @return LatLng for camera to be animated
     */
    private LatLng addMarkerToMap(QuakeItem item) {
        LatLng latLng = null;
        try {
            latLng = new LatLng(Double.parseDouble(item.latitude), Double.parseDouble(item.longitude));
        } catch (Exception e) {

        }

        if (latLng != null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker());
            markerOptions.title(item.region);
            markerOptions.snippet(item.magnitude + " @ depth: " + item.depth);

            CircleOptions circleOptions = new CircleOptions()
                    .center(latLng)
                    .radius(10000 * Double.parseDouble(item.magnitude))
                    .strokeColor(context.getResources().getColor(R.color.light_grey))
                    .fillColor(context.getResources().getColor(R.color.red));

            map.addCircle(circleOptions);

            return latLng;
        }

        return null;
    }

    /**
     * MOve the camera
     * @param latLng
     * @param duration
     * @param callback
     */
    private void moveCamera(LatLng latLng, Integer duration, GoogleMap.CancelableCallback callback) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 5);
        map.animateCamera(cameraUpdate, duration, callback);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();

        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ButterKnife.reset(this);

        mapView.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            context = (MainActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface that activity needs to implement to get all the data for animation
     */
    public interface OnFragmentInteractionListener {
        public QuakeData getData();
    }
}

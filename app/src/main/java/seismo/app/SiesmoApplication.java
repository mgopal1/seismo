package seismo.app;

import android.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

/**
 * Created by manoj on 01/12/2014.
 */
public class SiesmoApplication extends Application {

    private RequestQueue mRequestQueue;
    private static SiesmoApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static SiesmoApplication getInstance() {
        return SiesmoApplication.instance;
    }

    public RequestQueue getRequestQueue() {

        if (this.mRequestQueue == null) {
            this.mRequestQueue = Volley.newRequestQueue(this, new HurlStack());
        }

        return this.mRequestQueue;
    }
}

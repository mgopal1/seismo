package seismo.app.models.quake;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 01/12/2014.
 */
public class QuakeData implements Parcelable {

    public Integer count;

    @SerializedName("earthquakes")
    public List<QuakeItem> results = new ArrayList<QuakeItem>();


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.count);
        dest.writeTypedList(results);
    }

    public QuakeData() {
    }

    private QuakeData(Parcel in) {
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
        in.readTypedList(results, QuakeItem.CREATOR);
    }

    public static final Creator<QuakeData> CREATOR = new Creator<QuakeData>() {
        public QuakeData createFromParcel(Parcel source) {
            return new QuakeData(source);
        }

        public QuakeData[] newArray(int size) {
            return new QuakeData[size];
        }
    };
}

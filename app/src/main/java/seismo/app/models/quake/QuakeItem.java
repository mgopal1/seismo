package seismo.app.models.quake;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manoj on 01/12/2014.
 */
public class QuakeItem implements Parcelable {

    public String src;
    public String eqid;
    public String timedate;

    @SerializedName("lat")
    public String latitude;

    @SerializedName("lon")
    public String longitude;

    public String magnitude;
    public String depth;
    public String region;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.src);
        dest.writeString(this.eqid);
        dest.writeString(this.timedate);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.magnitude);
        dest.writeString(this.depth);
        dest.writeString(this.region);
    }

    public QuakeItem() {
    }

    private QuakeItem(Parcel in) {
        this.src = in.readString();
        this.eqid = in.readString();
        this.timedate = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.magnitude = in.readString();
        this.depth = in.readString();
        this.region = in.readString();
    }

    public static final Creator<QuakeItem> CREATOR = new Creator<QuakeItem>() {
        public QuakeItem createFromParcel(Parcel source) {
            return new QuakeItem(source);
        }

        public QuakeItem[] newArray(int size) {
            return new QuakeItem[size];
        }
    };
}
